# README #

RPMs for nvidia-container-toolkit.
See https://github.com/NVIDIA/nvidia-docker for details.

RPMs were downloaded by first adding https://nvidia.github.io/nvidia-docker/$distribution/nvidia-docker.repo to /etc/yum.repo.d/nvidia-docker.repo and

$ sudo yum install --downloadonly --downloaddir=. nvidia-container-toolkit

to download the RPMs to a given directory.